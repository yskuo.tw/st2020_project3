# -*- coding: utf-8 -*
import os  
import time
# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text
def test_SidebarContent():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	with open('window_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
	assert xmlString.find('查看商品分類') != -1

# 2. [Screenshot] Side Bar Text
def test_SidebarScreenshot():
	os.system('adb shell screencap -p /sdcard/screencap.png && adb pull /sdcard/screencap.png ./sidebarText.png')
	# os.system('adb exec-out screencap -p > sidebarText.png')

# 3. [Context] Categories
def test_hasCategoriesText():
	os.system('adb shell input keyevent 4')
	os.system('adb shell input swipe  500 1600 500 150 ')
	time.sleep(1)
	os.system('adb shell input tap 1000 300')

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	with open('window_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
	assert xmlString.find('選擇分類') != -1
# 4. [Screenshot] Categories
def test_CategoriesScreenShot():
	os.system('adb shell screencap -p /sdcard/screencap.png') 
	os.system('adb pull /sdcard/screencap.png ./categories.png')
# 5. [Context] Categories page
def test_CategoriesPageExist():
	os.system('adb shell input tap 300 1700')
	os.system('adb shell input tap 300 1700')
	os.system('adb shell input tap 300 1700')

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	with open('window_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
	assert xmlString.find('24H購物') != -1
	assert xmlString.find('購物中心') != -1

# 6. [Screenshot] Categories page
def test_CategoriesPageScreenShot():
	os.system('adb shell screencap -p /sdcard/screencap.png') 
	os.system('adb pull /sdcard/screencap.png ./categories_page.png')
# 7. [Behavior] Search item “switch”
def test_searchItem():
	os.system('adb shell input tap 400 140')

	time.sleep(0.5)
	os.system('adb shell input text "switch"')
	time.sleep(0.5)
	os.system('adb shell input keyevent "KEYCODE_ENTER"')
# 8. [Behavior] Follow an item and it should be add to the list
def test_followItem():
	time.sleep(2)
	os.system('adb shell input tap 700  650')
	time.sleep(2)
	os.system('adb shell input tap 75 1715')
	time.sleep(2)
	os.system('adb shell input keyevent 4')
	time.sleep(1)
	os.system('adb shell input tap 100  1700')

	time.sleep(1.5)

	os.system('adb shell input tap 100 100')
	time.sleep(2.5)
	os.system('adb shell input tap 350  900')
	time.sleep(1)

	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	with open('window_dump.xml', 'r', encoding="utf-8") as f:
		xmlString = f.read()
	assert xmlString.lower().find('switch') != -1
# 9. [Behavior] Navigate tto the detail of item
def test_NavigatetoDetail():
	os.system('adb shell input tap 300 1000')
	time.sleep(2)
	os.system('adb shell input swipe 630 920  630 420')
	time.sleep(0.5)
	os.system('adb shell input tap 500 100')
# # 10. [Screenshot] isconnetion Screen
def test_DisconnetionScreenShot():
	time.sleep(5)
	os.system('adb shell input keyevent 4')
	time.sleep(1)

	os.system('adb shell input keyevent 4')
	time.sleep(1)

	os.system('adb shell svc  wifi disable')
	time.sleep(0.5)

	os.system('adb shell svc  data disable')
	time.sleep(0.5)
	print('go to notification')
	os.system('adb shell input tap 525 1690')
	time.sleep(1)
	print('take a sreenshot!')

	os.system('adb shell screencap -p /sdcard/screencap.png') 
	os.system('adb pull /sdcard/screencap.png ./disconnection.png')
